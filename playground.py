import cv2
import pytesseract
import numpy as np

from PIL import Image

import os
import pathlib

i = 1

# def cards_from_image(image):
#     global i
#     i = i + 1
#
#     card1 = image[348:406, 524:550]
#     card2 = image[348:406, 552:589]
#
#     table1 = image[213:260, 368:402]
#     table2 = image[213:260, 413:450]
#     table3 = image[213:260, 459:496]
#     table4 = image[213:260, 504:541]
#     table5 = image[213:260, 552:585]
#
#     # card1 = Image.fromarray(card1, mode='RGB')
#     # card2 = Image.fromarray(card2, mode='RGB')
#
#     table1 = Image.fromarray(table1, mode='RGB')
#     table2 = Image.fromarray(table2, mode='RGB')
#     table3 = Image.fromarray(table3, mode='RGB')
#     table4 = Image.fromarray(table4, mode='RGB')
#     table5 = Image.fromarray(table5, mode='RGB')
#
#     # card1.save("cards/card1/card" + str(i) + ".png")
#     # card2.save("cards/card2/card" + str(i) + ".png")
#
#     # table1.save("cards/table1/card" + str(i) + ".png")
#     # table2.save("cards/table2/card" + str(i) + ".png")
#     # table3.save("cards/table3/card" + str(i) + ".png")
#     table4.save("cards/table4/card" + str(i) + ".png")
#     # table5.save("cards/table5/card" + str(i) + ".png")
#
def extract_from_tournament(directory):
    if directory.decode() == "archive":
        return

    path = "cards/table3/final/" + directory.decode() + "/"
    directory = os.fsencode(path)

    print(directory.decode())
    for file in os.listdir(directory):
        img = cv2.imread(path + file.decode())
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = np.array(img)
        table3 = img[0:47, 0:14]
        table3 = Image.fromarray(table3, mode='RGB')
        table3.save(path + file.decode())

directory = os.fsencode("cards/table3/final")

for dir in os.listdir(directory):
    extract_from_tournament(dir)



# card1 = cv2.cvtColor(cv2.imread("cards/card1.png"), cv2.COLOR_BGR2RGB)
# card2 = cv2.cvtColor(cv2.imread("cards/card2.png"), cv2.COLOR_BGR2RGB)
# test = cv2.cvtColor(cv2.imread("cards/test.png"), cv2.COLOR_BGR2RGB)
#
# player1 = test[348:406, 524:550]
# player2 = test[348:406, 552:589]
#
# difference = cv2.subtract(player1, card2)
# result = not np.any(difference)
#
# if result:
#     print("DA")










# --- Load cards -----

# loading all the cards into the Window memory (even if it's not that optimal)
# def loadCardsTemplates():
#     colors = ["blue", "gray", "green", "red"]
#     types = ["card1", "card2"]
#     # types = ["card1", "card2", "table1", "table2", "table3", "table4", "table5"]
#     numbers = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "Y", "J", "Q", "K"]
#     cards = {}
#
#     for type in types:
#         path = "cards/" + type + "/"
#         cards[type] = {}
#         for color in colors:
#             cards[type][color] = {}
#             for number in numbers:
#                 cards[type][color][number] = cv2.imread(path + "final/" + color + "/" + number + ".png")
#
# loadCardsTemplates()


