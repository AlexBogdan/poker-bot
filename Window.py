import extraction as extraction
import win32gui
import pyautogui
import pyscreenshot as ImageGrab
from datetime import datetime
import os
import cv2
import numpy as np
import imagehash
import random
from PIL import Image


# TODO 1: getHandId
# TODO 2: find how much did everyone bet
# TODO 3: find how much does everyone has in their balance

from Image import WindowImage


class Window:
    # 0 level function - the Window init

    # What areas are important in the game window. Usually, the important areas are the text ones and the cards.
    # Buttons.
    # *The Check button is formed by combining the FOLD and CALL buttons
    FOLD_BUTTON = [355, 455, 426, 500]
    CALL_BUTTON = [433, 455, 526, 500]
    BET_BUTTON = [531, 455, 620, 500]

    # Some numerical values that appear on the table during the game and are crucial
    POT_VALUE = [390, 184, 550, 207]
    CALL_VALUE = [444, 477, 515, 495]
    BET_VALUE = [535, 436, 615, 451]

    # Game stats = any information that isn't directly related to a decision making process
    PLAYER_BANK = [465, 373, 512, 390]
    HAND_ID = [106, 43, 168, 57]

    # Cards - We only care for the Bot's cards and what it's on the table
    PLAYER_CARD1 = [524, 348, 550, 406]
    PLAYER_CARD2 = [552, 348, 589, 406]

    TABLE_CARD1 = [368, 213, 402, 260]
    TABLE_CARD2 = [413, 213, 450, 260]
    TABLE_CARD3 = [459, 213, 473, 260]
    TABLE_CARD4 = [504, 213, 541, 260]
    TABLE_CARD5 = [552, 213, 585, 260]

    def __init__(self, id):
        # Define Tournament as a Window
        self.id = id
        self.name = None
        self.handle = None
        self.coordinates = None
        self.findWindow()

        # Create the tournament folder in the archive
        path = "tournaments/" + str(self.id)
        if not os.path.exists(path):
            os.makedirs(path)

        # Load the card templates
        self.cardTemplates = {}  # player's cards and cards that are on the table
        self.loadCardTemplates()

        # Game state

        self.ss: WindowImage  # the current game screenshot
        self.bigBlind = None
        self.handId = None
        self.playerBank = None

        self.playerCards = []

    # Find the handle for the specified window
    def findWindow(self):
        def findWindowHandler(hwnd, ctx):
            if win32gui.IsWindowVisible(hwnd):
                name = win32gui.GetWindowText(hwnd)
                if "(" + self.id + ")" in name:
                    self.name = name
                    self.handle = hwnd

        win32gui.EnumWindows(findWindowHandler, None)

    # load the player cards and also the table cards
    def loadCardTemplates(self):
        templateTypes = ["card1", "card2", "table1", "table2", "table3", "table4", "table5"]
        colors = [("blue", "d"), ("achro", "s"), ("green", "c"), ("red", "h")]
        numbers = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"]
        self.cardTemplates = {}

        for templateType in templateTypes:
            path = "cards/" + templateType + "/"
            self.cardTemplates[templateType] = {}
            for (color, type) in colors:
                self.cardTemplates[templateType][color] = {}
                for number in numbers:
                    self.cardTemplates[templateType][color][number + type] = \
                        cv2.imread(path + "final/" + color + "/" + number + type + ".png", cv2.COLOR_BGR2RGB)

        # print("load complete")
        # cv2.imshow("card", self.cards["card1"]["achro"]["As"])
        # cv2.waitKey(0)

    # -1- first level functions - the most important ones that are used even out of the class

    # Extract the current game state.
    # For a better timing, this function will be called by the bot when he needs to choose an action
    def updateStats(self):
        # Get a new screenshot of the game
        self.updateWindowCoordinates()
        self.updateWindowImage()

        self.handId = self.getHandId()
        self.playerBank = self.getPlayerBank()
        # self.bigBlind = self.getBigBlind()

    # checks if there is an action required on the table
    def checkActionRequest(self):
        # check if the action box is present
        foldButton = self.ss.crop(*Window.FOLD_BUTTON).dominantColor()
        if foldButton == "red":
            return True

        callButton = self.ss.crop(*Window.CALL_BUTTON).dominantColor()
        if callButton == "orange":
            return True

        betButton = self.ss.crop(*Window.BET_BUTTON).dominantColor()
        if betButton == "green":
            return True

        return

    def isFlop(self):
        color1 = self.ss.crop(*Window.TABLE_CARD1).dominantColor()
        print("color1 - " + color1)

        return color1 == "red" or color1 == "green" or color1 == "blue" or color1 == "achro"

    def isTurn(self):
        color4 = self.ss.crop(*Window.TABLE_CARD4).dominantColor()
        print("color2- " + color4)

        return color4 == "red" or color4 == "green" or color4 == "blue" or color4 == "achro"

    def isRiver(self):
        color5 = self.ss.crop(*Window.TABLE_CARD5).dominantColor()
        print("color3 - " + color5)

        return color5 == "red" or color5 == "green" or color5 == "blue" or color5 == "achro"

    def getPossibleActions(self):
        foldButton = self.ss.crop(*Window.FOLD_BUTTON).dominantColor()
        callButton = self.ss.crop(*Window.CALL_BUTTON).dominantColor()
        betButton = self.ss.crop(*Window.BET_BUTTON).dominantColor()

        actions = {}

        if foldButton == "red":
            actions["fold"] = -self.getPotValue()

        if foldButton == "orange" and callButton == "orange":
            actions["check"] = 0

        if foldButton == "red" and callButton == "orange":
            actions["call"] = self.getCallValue()

        if betButton == "green":
            actions["bet"] = self.getBetValue()

        return actions

    def applyAction(self, action):
        self.saveWindowImage()

        if action == "fold":
            point_x = random.randrange(Window.FOLD_BUTTON[0], Window.FOLD_BUTTON[2])
            point_y = random.randrange(Window.FOLD_BUTTON[1], Window.FOLD_BUTTON[3])

            pyautogui.moveTo(point_x, point_y, random.randrange(10, 25) / 100)
            pyautogui.click(point_x, point_y)

        if action == "check":
            point_x = random.randrange(Window.FOLD_BUTTON[0], Window.CALL_BUTTON[2])
            point_y = random.randrange(Window.FOLD_BUTTON[1], Window.CALL_BUTTON[3])

            pyautogui.moveTo(point_x, point_y, random.randrange(10, 25) / 100)
            pyautogui.click(point_x, point_y)

        pyautogui.moveTo(960, 525, random.randrange(5, 10) / 100)

    # -2- second level functions - those that are used inside the class and that are not that important
    # find where the window is currently located
    def updateWindowCoordinates(self):
        self.coordinates = win32gui.GetWindowRect(self.handle)

    # Take a screenshot for the current Window and save it in its folder
    def updateWindowImage(self):
        self.updateWindowCoordinates()
        ss = ImageGrab.grab(bbox=self.coordinates)
        self.ss = WindowImage(np.array(ss))

    def saveWindowImage(self):
        path = "tournaments/" + str(self.id) + "/"
        self.ss.saveImage(path + str(datetime.now().strftime('%H_%M_%S')) + '.png')

    def textToNumber(self, number):
        number = number[:-2]
        if "," in number:
            number = number.replace(",", "")

        if number.replace('.', '', 1).isdigit():
            return float(number)

        return 0

    def extractText(self, area, resize_f=4, dilate_f=1, threshold=180, show=False):
        textCrop = self.ss.crop(*area)
        textCrop.resize(resize_f)
        textCrop.contrast()
        textCrop.grayScale()
        textCrop.threshold(threshold=threshold)
        textCrop.blur()
        textCrop.dilate(dilate_f)
        textCrop.resize(0.75)
        if show:
            textCrop.showImage()

        return textCrop.getText()

    def getPotValue(self):
        return self.textToNumber(self.extractText(Window.POT_VALUE, threshold=215))

    def getCallValue(self):
        return self.textToNumber(self.extractText(Window.CALL_VALUE, threshold=180))

    def getBetValue(self):
        return self.textToNumber(self.extractText(Window.BET_VALUE, threshold=180))

    def getPlayerBank(self):
        return self.textToNumber(self.extractText(Window.PLAYER_BANK, threshold=180))

    def getHandId(self):
        return self.textToNumber(self.extractText(Window.HAND_ID, resize_f=6, dilate_f=0, threshold=105))

    def getBigBlind(self):
        for text in self.name.split(" - "):
            if "/" in text:
                return float(text.split("/")[1])

        return None

    def getCard(self, type):
        if type == "card1":
            area = Window.PLAYER_CARD1
        elif type == "card2":
            area = Window.PLAYER_CARD2
        elif type == "table1":
            area = Window.TABLE_CARD1
        elif type == "table2":
            area = Window.TABLE_CARD2
        elif type == "table3":
            area = Window.TABLE_CARD3
        elif type == "table4":
            area = Window.TABLE_CARD4
        elif type == "table5":
            area = Window.TABLE_CARD5
        else:
            return None

        card = self.ss.crop(*area)
        color = card.dominantColor()

        if color not in self.cardTemplates[type].keys():
            return None

        card.toRGB()
        for (number, cardTemplate) in self.cardTemplates[type][color].items():
            # difference = cv2.subtract(cardTemplate, card.image)
            hash0 = imagehash.average_hash(Image.fromarray(card.image))
            hash1 = imagehash.average_hash(Image.fromarray(cardTemplate))
            diff = hash0 - hash1
            cutoff = 0.5

            if diff < cutoff:
                return number

        return None

    # the result will be a dict with all the cards that are on the table + player's hand
    def getCards(self):
        return {type: self.getCard(type) for type in self.cardTemplates.keys()}

    def __str__(self):
        return self.name
