import cv2
import numpy as np
import color_extraction
import pytesseract
from PIL import Image

# functions that transform an image
class WindowImage(object):
    # the image should be on np.array format
    def __init__(self, image):
        self.image = image

    def crop(self, x0, y0, x1, y1) -> "WindowImage":
        return WindowImage(self.image[y0:y1, x0:x1])

    def contrast(self):
        lab = cv2.cvtColor(self.image, cv2.COLOR_BGR2LAB)
        l, a, b = cv2.split(lab)
        clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
        cl = clahe.apply(l)
        limg = cv2.merge((cl, a, b))
        self.image = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

    def resize(self, f=2.0):
        self.image = cv2.resize(self.image, None, fx=f, fy=f, interpolation=cv2.INTER_CUBIC)

    def dilate(self, factor=1):
        kernel = np.ones((factor, factor), np.uint8)
        self.image = cv2.dilate(self.image, kernel, iterations=1)

    def erode(self, factor=1):
        kernel = np.ones((factor, factor), np.uint8)
        self.image = cv2.erode(self.image, kernel, iterations=1)

    def blur(self, factor=1):
        self.image = cv2.medianBlur(self.image, factor)

    def grayScale(self):
        self.image = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)

    def toRGB(self):
        self.image = cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB)

    def threshold(self, threshold=127, type="binary"):
        if type == "binary":
            self.image = cv2.threshold(self.image, threshold, 255, cv2.THRESH_BINARY)[1]
        elif type == "otsu":
            self.image = cv2.threshold(self.image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    def dominantColor(self):
        colors = color_extraction.get_counts(self.image)
        return max(colors, key=colors.get)

    def showImage(self):
        cv2.imshow("image", self.image)
        cv2.waitKey(0)

    def saveImage(self, path):
        im = Image.fromarray(self.image)
        im.save(path)

    def getText(self):
        return pytesseract.image_to_string(self.image, config="--psm 6 --oem 3 -c tessedit_char_whitelist=0123456789")



# img = cv2.imread("preflop.png")
# img = np.array(img)
# image = WindowImage(img)
#
# call = image.crop(444, 477, 515, 495)
# bet = image.crop(535, 436, 615, 451)
# pot = image.crop(390, 184, 550, 207)
#
# call.resize(4)
# call.contrast()
# call.grayScale()
# call.threshold(threshold=215)
# call.blur()
# call.dilate(1)
# call.resize(3/4)
# # call.showImage()
# print(call.getText())
#
# bet.resize(4)
# bet.contrast()
# bet.grayScale()
# bet.threshold(threshold=180)
# bet.blur()
# bet.dilate(1)
# bet.resize(3/4)
# # bet.showImage()
# print(bet.getText())
#
#
# pot.resize(4)
# pot.contrast()
# pot.grayScale()
# pot.threshold(threshold=125)
# pot.blur()
# pot.dilate(1)
# pot.resize(3/4)
# # pot.showImage()
# print(pot.getText())
