# The BetBot, one of the Prophets of Fortune
# His behavior should imitate a real player, more precisely a poker player
from Window import Window
import win32gui

from typing import Dict
import time

import holdem_calc


class BetBot:
    # There will be max 4 windows in a view for a 1080p screen
    WindowCoordinates = {
        1: ((0, 0), (955, 520)),
        2: ((0, 520), (955, 520)),
        3: ((965, 0), (955, 520)),
        4: ((965, 520), (955, 520)),
    }

    def __init__(self):
        # Define other stats_variables
        self.balance = None
        self.tickets = []

        # Define ongoing tournaments
        self.tournaments: Dict[int, Window] = dict()

    # move each window in its place on the screen
    def replaceWindowsOnScreen(self):
        for i in self.tournaments.keys():
            window = self.tournaments[i]
            ((x0, y0), (x1, y1)) = BetBot.WindowCoordinates[i]
            win32gui.MoveWindow(window.handle, x0, y0, x1, y1, True)

    def add_tournament(self, id):
        for i in range(1, 5):
            if i not in self.tournaments.keys():
                self.tournaments[i] = Window(id)
                break
        self.replaceWindowsOnScreen()

    def playPoker(self):
        while True:
            for tournament in self.tournaments.values():
                tournament.updateStats()
                if tournament.checkActionRequest():
                    actions = tournament.getPossibleActions()

                    print(actions)

                    # if "fold" in actions.keys():
                    #     tournament.applyAction("fold")
                    # elif "check" in actions.keys():
                    #     tournament.applyAction("check")

            for tournament in self.tournaments.values():
                tournament.updateStats()
                cards = tournament.getCards()

                boardCards = [card for card in list(cards.values())[2:] if card is not None]
                if boardCards == []:
                    boardCards = None

                playerCards = [card for card in list(cards.values())[:2] if card is not None]
                if len(playerCards) == 0:
                    continue
                playerCards += ["?", "?"]

                if boardCards == None or len(boardCards) < 3:
                    exact = False
                else:
                    exact = True

                winOdds = holdem_calc.calculate(board=boardCards, exact=exact, num=1000, input_file=None,
                                                hole_cards=playerCards, verbose=True)
                print(playerCards)
                print(boardCards)
                print(winOdds)

            time.sleep(5)

    # When asked, the bot will provide his name and his stats as a poker player
    def __str__(self):
        res = "PokerBot - MasterChie14 " + "\n"

        res += "\nTournaments currently enrolled in:\n"
        for tournament in self.tournaments.values():
            res += str(tournament) + "\n"

        return res


bot = BetBot()
bot.add_tournament("20552648")
bot.playPoker()
